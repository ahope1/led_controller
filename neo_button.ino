
// Cool ideas: 

// FIRE! 
// https://www.tweaking4all.com/hardware/arduino/adruino-led-strip-effects/#LEDStripEffectFire



#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> 
#endif

#define FIRE_LEDS 4 // Pin that drives the LEDs that show the firing
#define FIRE_LED_COUNT 30 // Number of LEDs on the strip that shows firing
#define FIRE_INPUT_PIN 2 // Pin that specifies if the FIRE LED strip should be showing

#define ALLIANCE_INPUT_PIN 0 // Pin that specifies if we are on the Red alliance or not
#define ALLIANCE_LEDS 3
#define ALLIANCE_LED_COUNT 30 

Adafruit_NeoPixel strip(FIRE_LED_COUNT, FIRE_LEDS, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel redBlueStrip(ALLIANCE_LED_COUNT, ALLIANCE_LEDS, NEO_GRB + NEO_KHZ800);

    

void setup() {
  strip.begin();          
  strip.show();            
  strip.setBrightness(255); 
  
  redBlueStrip.begin();
  redBlueStrip.show();
  redBlueStrip.setBrightness(255);

  pinMode(FIRE_INPUT_PIN, INPUT);
  pinMode(ALLIANCE_INPUT_PIN, INPUT);
}

int isRed = 0;
int buttonState = 0;    
int deez = 0;


void loop() {

  buttonState = digitalRead(FIRE_INPUT_PIN);
  isRed = digitalRead(ALLIANCE_LEDS);

  if (isRed) {
    colorWipeRedBlue(redBlueStrip.Color(  255,   0, 0), 50); // Red
  } else {
    colorWipeRedBlue(redBlueStrip.Color(  0,   0, 255), 50); // Blue
  }

  if (buttonState == HIGH) {
   // theaterChase(strip.Color(0, 255, 255), 50);
   //   meetInTheMiddle(strip.Color(0, 255, 255), 50, 30);
   RainbowMeetInMeedle(50);
  } else {
    if (isRed) {
      colorWipe(strip.Color(255,   0,   0), 50);
    }
    else {
      colorWipe(strip.Color(0,   0,   255), 50);
    }
  }
}

// This function needs to be tested
// It should replace the other colorWipe functions, but needs to be confirmed
void colorWipeTest(Adafruit_NeoPixel strip, uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { 
    strip.setPixelColor(i, color);         
    strip.show();                         
    delay(wait);                          
  }
}

void colorWipeRedBlue(uint32_t color, int wait) {
  for(int i=0; i<redBlueStrip.numPixels(); i++) { 
    redBlueStrip.setPixelColor(i, color);         
    redBlueStrip.show();                         
    //delay(wait);                          
  }
}

void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { 
    strip.setPixelColor(i, color);         
    strip.show();                          
    //delay(wait);                         
  }
}

void theaterChase(uint32_t color, int wait) {
  //for(int a=0; a<10; a++) {  
    for(int b=0; b<3; b++) { 
      strip.clear();        
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color);
      }
      strip.show(); 
      delay(wait);  
    }
  //}
}

void theaterChaseMeetInTheMiddle(uint32_t color, int wait) {
  //for(int a=0; a<10; a++) {  
  int middle = strip.numPixels()/2;
    for(int b=0; b<3; b++) { 
      strip.clear();        
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color);
        strip.setPixelColor(strip.numPixels()-c-1, color);
      }
      strip.show(); 
      delay(wait);  
    }
  //}
}

void RainbowMeetInMeedle(int wait) {
  int firstPixelHue = 0; 
  strip.clear();
  int middles = strip.numPixels()/2;
  for(int a = 0; a < middles; a++) {
    int      hue   = firstPixelHue + a * 65536L / strip.numPixels();
    uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
    strip.setPixelColor(a, color);
    strip.setPixelColor(strip.numPixels() - a - 1, color);
    strip.show();
    delay(wait);
    firstPixelHue += 65536 / 30; // One cycle of color wheel over 90 frames
    
  }
}

// This didn't work so well in practice. Not sure what needs to be done to make it nicer. 
void theaterChaseMeetInTheMiddleRAINBOW(int wait) {
   int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  //for(int a=0; a<10; a++) {  
  int middle = strip.numPixels()/2;
    for(int b=0; b<3; b++) { 
      strip.clear();        
      for(int c=b; c<strip.numPixels(); c += 3) {
        int      hue   = firstPixelHue + c * 65536L / strip.numPixels();
        uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
        strip.setPixelColor(c, color);
        strip.setPixelColor(strip.numPixels()-c-1, color);
      }
      strip.show(); 
      delay(wait);  
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  //}
}


void meetInTheMiddle(uint32_t color, int wait, int numPixels) {
  strip.clear();
  int last = numPixels / 2;
  for(int a = 0; a < last; a++) {
    strip.setPixelColor(a, color); 
    strip.setPixelColor(numPixels - a - 1, color);
    strip.show();
    delay(wait);
  }
}

void rainbow(int wait) {
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    strip.rainbow(firstPixelHue);
    strip.show(); 
    delay(wait); 
  }
}



